function replaceDiscordThings(source_string)
{
    if (source_string == null)
        return '';

    replaceChannel = [];
    replaceUser = [];
    replaceChannel.push(['&lt;#96028420848230400&gt;',  '#tech-support', '96028420848230400']);
    replaceChannel.push(['&lt;#497150397358800908&gt;', '#build-help', '497150397358800908']);
    replaceChannel.push(['&lt;#96037009314836480&gt;',  '#off-topic-memes-and-stuff', '96037009314836480']);
    replaceChannel.push(['&lt;#77710284621357056&gt;',  '#main', '77710284621357056']);
    replaceChannel.push(['&lt;#537354814665785355&gt;', '#roles', '537354814665785355']);
    replaceChannel.push(['&lt;#500820755391709194&gt;', '#overclocking', '500820755391709194']);
    replaceChannel.push(['&lt;#135645254014468096&gt;', '#programming', '135645254014468096']);
    replaceChannel.push(['&lt;#213077219424206858&gt;', '#linux', '213077219424206858']);
    replaceChannel.push(['&lt;#213077236683636736&gt;', '#mac-os', '213077236683636736']);
    replaceChannel.push(['&lt;#750325966936473600&gt;', '#hardware-discussion', '750325966936473600']);
    replaceUser.push(['&lt;@630852264780890135&gt;', '@PCMRBot']);
    replaceUser.push(['&lt;@355619002569195520&gt;', '@Modmail']);

    for (let i = 0; i < replaceChannel.length; i++) {
        source_string = source_string.replaceAll(replaceChannel[i][0], `<a href="https://discord.com/channels/77710284621357056/${replaceChannel[i][2]}" target="_blank" class="discord_replace">${replaceChannel[i][1]}</a>`);
    }
    for (let i = 0; i < replaceUser.length; i++) {
        source_string = source_string.replaceAll(replaceUser[i][0], `<span class="discord_replace">${replaceUser[i][1]}</span>`);
    }

    return source_string
}
marked.setOptions({ gfm: true, breaks: true });

$(document).ready(function($){
    pcmrbot_prefix = "!";
    $("#wrapper tfoot").hide();
    $("#loading").show();
    $.ajax({
        url: './commands.json?t='+Date.now()
    }).done(function(data){
        $("#search_string").focus();
        i = 0;
        theTable = $("#wrapper table tbody");
        for (const [command, command_data] of Object.entries(data)) {
            if (command_data.hasOwnProperty("attribs") && command_data['attribs'].includes("hidden"))
                continue

            i += 1;
            theimage = command_data['image'];
            if (command_data.hasOwnProperty("image") && theimage.startsWith("/"))
            {
                theimage = `https://gitlab.com/pcmasterrace/discord/pcmr-discord-bot-commands/-/raw/master${theimage}`;
            }
            aliases = (command_data.hasOwnProperty("aliases")) ? `<br /><span class="aliases">${command_data['aliases'].join("<br />")}</span>` : ``;
            txt = (command_data.hasOwnProperty("text")) ? `<div class="command_text">${replaceDiscordThings(marked.parse(command_data['text']))}</div>` : ``;
            img = (command_data.hasOwnProperty("image")) ? `<div class="command_image"><img src="${theimage}" href="${theimage}" class="image-lightbox"/></div>` : ``;
            // flags = (command_data.hasOwnProperty("attribs")) ? `${command_data['attribs'].join("<br />")}` : ``;
            flags = '';


            theTable.append(`<tr>`+
            `    <td class="command"><a name="${command.toLowerCase()}" class="anchor"></a><a href="#${command.toLowerCase()}">${command.toLowerCase()}</a>${aliases}<div class="flags">${flags}</div></td>`+
            `    <td class="name"><div class="command_name">${replaceDiscordThings(command_data['name'])}</div>${txt}${img}</td>`+
            `</tr>`);
        }
        
        $('.image-lightbox').featherlight({type: 'image'});

        $("#number_of_commands").html(i);

        $("#loading").hide();

        if (document.location.hash != "")
            $(`a[name="${document.location.hash.substring(1)}"]`)[0].scrollIntoView(true);
    });
    $("#search_string").keyup(function(){
        var visible_commands = 0;
        search_string = $("#search_string").val().toLowerCase();
        theTable = $("#wrapper table tbody").find("tr").each(function(){
            if ($(this).text().toLowerCase().includes(search_string))
            {
                visible_commands += 1;
                $(this).show();
            }
            else
            {
                $(this).hide();
            }
        });

        if (visible_commands == 0)
            $("#wrapper tfoot").show();
        else
            $("#wrapper tfoot").hide();
    });
});
